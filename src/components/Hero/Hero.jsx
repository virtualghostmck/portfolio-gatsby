import React, { useContext, useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import Fade from 'react-reveal/Fade';
import { Link } from 'react-scroll';
import Typewriter from 'typewriter-effect';
import Particles from 'react-particles-js';
import PortfolioContext from '../../context/context';

const Header = () => {
  const { hero } = useContext(PortfolioContext);
  const { title, name, cta } = hero;

  const [isDesktop, setIsDesktop] = useState(false);
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    if (window.innerWidth > 769) {
      setIsDesktop(true);
      setIsMobile(false);
    } else {
      setIsMobile(true);
      setIsDesktop(false);
    }
  }, []);
  // TODO Add animation of moving images in loop.(cards swapping animation)
  // TODO Show animated subtitle with typing animation.
  // TODO Change the button text of know more.

  return (
    <section id="hero" className="jumbotron">
      <Particles
        params={{
          particles: {
            number: {
              value: 30,
            },
            size: {
              value: 7,
            },
          },
          interactivity: {
            events: {
              onhover: {
                enable: true,
                mode: 'repulse',
              },
            },
          },
        }}
        height="100vh"
        className="MyParticleBg"
      />
      <Container>
        <Fade left={isDesktop} bottom={isMobile} duration={1000} delay={500} distance="30px">
          <h1 className="hero-title">
            {title || 'Hi, my name is'}{' '}
            <span className="text-color-main">{name || 'Your Name'}</span>
            {/* <br />
            {subtitle || "I'm the Unknown Developer."} */}
          </h1>
          <div className="hero-title">
            <Typewriter
              onInit={(typewriter) => {
                typewriter
                  .typeString(
                    "I'm a <span style='background-image: linear-gradient(135deg, #833ab4 0%, #fd1d1d 100%);-webkit-background-clip: text;-webkit-text-fill-color: transparent;'>React Developer</span>"
                  )
                  .pauseFor(1000)
                  .deleteChars(15)
                  .typeString(
                    '<span style="background-image: linear-gradient(135deg, #833ab4 0%, #fd1d1d 100%);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">.NET Developer</span>'
                  )
                  .pauseFor(1000)
                  .deleteChars(14)
                  .typeString(
                    '<span style="background-image: linear-gradient(135deg, #833ab4 0%, #fd1d1d 100%);-webkit-background-clip: text;-webkit-text-fill-color: transparent;">Full Stack Developer</span>'
                  )
                  .start();
              }}
            />
          </div>
        </Fade>
        <Fade left={isDesktop} bottom={isMobile} duration={1000} delay={500} distance="30px">
          <p className="hero-cta">
            <span className="cta-btn cta-btn--hero">
              <Link to="about" smooth duration={1000}>
                {cta || 'Know more'}
              </Link>
            </span>
          </p>
        </Fade>
      </Container>
    </section>
  );
};

export default Header;

// element.style {
//   position: absolute;
//   top: 0;
//   left: 0;
//   height: 100%;
//   width: 100%;
// }
