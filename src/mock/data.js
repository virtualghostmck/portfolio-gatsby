import { nanoid } from 'nanoid';

// HEAD DATA
export const headData = {
  title: 'Mayuresh Kakade | Full Stack Developer', // e.g: 'Name | Developer'
  lang: 'en', // e.g: en, es, fr, jp
  description: '', // e.g: Welcome to my website
};

// HERO DATA
export const heroData = {
  title: '',
  name: 'Mayuresh Kakade',
  subtitle: '',
  cta: '',
};

// ABOUT DATA
export const aboutData = {
  img: 'profile.jpg',
  paragraphOne:
    'Passionate engineering professional with 3 years of experience in software analysis, design, development and testing using the latest and befitting technology for various business solutions.',
  paragraphTwo:
    'Skilled in Full-Stack Development, React, Redux, .NET core 2.2, MS SQL Server, and C#. Involved in intuitive approach of problem solving.',
  paragraphThree:
    'A Strong engineering professional with a Bachelor of Engineering - BE focused in Computer Science from Maharashtra Institute of Technology.',
  resume:
    'https://media-exp1.licdn.com/dms/document/C4E2DAQGnzd37e_-PZg/profile-treasury-document-pdf-analyzed/0/1618036650072?e=1622998800&v=beta&t=5roXIWWB4JJL4sVI0YgCqvCT_crel1yZ0dezcz7q79Y', // if no resume, the button will not show up
};

// PROJECTS DATA
export const projectsData = [
  {
    id: nanoid(),
    img: 'project.jpg',
    title: 'Chat Application',
    info: 'A web-based Slack-like chat application developed using ReactJs, Material UI and Firebase. The application allows users to create channels, have discussions and send text messages as well as images to each other',
    info2: '',
    url: 'https://chat-box-clone.firebaseapp.com/',
    repo: 'https://gitlab.com/virtualghostmck/chat-app', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'project.jpg',
    title: 'Photography Portfolio Website',
    info: 'Photography Portfolio Website built using React JS and styled with Bootstrap. Demonstrates lazy loading of images and components. CICD pipeline setup is made using firebase for Gitlab.',
    info2: '',
    url: 'https://www.omkarkalgude.in/',
    repo: 'https://gitlab.com/photography-portfolio/photography-portfolio-site', // if no repo, the button will not show up
  },
  // {
  //   id: nanoid(),
  //   img: 'project.jpg',
  //   title: '',
  //   info: '',
  //   info2: '',
  //   url: '',
  //   repo: 'https://github.com/cobidev/react-simplefolio', // if no repo, the button will not show up
  // },
];

// CONTACT DATA
export const contactData = {
  cta: '',
  btn: '',
  email: '',
};

// FOOTER DATA
export const footerData = {
  networks: [
    {
      id: nanoid(),
      name: 'linkedin',
      url: 'https://www.linkedin.com/in/mayuresh-kakade-ba4658125/',
    },
    {
      id: nanoid(),
      name: 'gitlab',
      url: 'https://gitlab.com/users/virtualghostmck/starred',
    },
    {
      id: nanoid(),
      name: 'codepen',
      url: 'https://codesandbox.io/u/virtualghostmck',
    },
  ],
};

// Github start/fork buttons
export const githubButtons = {
  isEnabled: true, // set to false to disable the GitHub stars/fork buttons
};
